#!/usr/bin/env python

import argparse
import sys, os
import math
import logging
from Bio import SeqIO
import graphviz
import time
from collections import deque


def get_km1mer_histogram(reads, k):
    ''' Return k-mer count '''
    kmerhist = {}
    if reads:
        for read in reads:
            for kmer in [ read[i:i+k-1] for i in range(len(read)+1-(k-1)) ]:
                kmerhist[kmer] = kmerhist.setdefault(kmer, 0) + 1
    return kmerhist

class DeBruijnGraph:
    ''' De Bruijn directed multigraph built from a collection of
        strings. User supplies strings and k-mer length k.  Nodes
        are k-1-mers.  An Edge corresponds to the k-mer that joins
        a left k-1-mer to a right k-1-mer. '''
 
    @staticmethod
    def chop(st, k):
        ''' Chop string into k-mers of given length '''
        for i in range(len(st)-(k-1)):
            yield (st[i:i+k], st[i:i+k-1], st[i+1:i+k])
    
    class Node:
        ''' Node representing a k-1 mer.  Keep track of # of
            incoming/outgoing edges so it's easy to check for
            balanced, semi-balanced. '''
        
        def __init__(self, km1mer):
            self.km1mer = km1mer
            self.in_nodes = []
            self.out_nodes = [] 
            self.freq = 0
            
        @property
        def nin(self):
            return len(self.in_nodes)
        
        @property
        def nout(self):
            return len(self.out_nodes)
        
        def isSemiBalanced(self):
            return abs(self.nin - self.nout) == 1
        
        def isBalanced(self):
            return self.nin == self.nout
        
        def __hash__(self):
            return hash(self.km1mer)
        
        def __str__(self):
            return self.km1mer
        
        def print_node(self):
            return f"Node {self.km1mer}\n  -freq: {self.freq}" + \
                    f"\n  -in nodes: {[str(n) for n in self.in_nodes]} {self.nin}"+ \
                    f"\n  -out nodes: {[str(n) for n in self.out_nodes]} {self.nout} " +\
                    f"\n  -is Balanced: {self.isBalanced()}" +\
                    f"\n  -is Semi-Balanced: {self.isSemiBalanced()}" 
        
        def remove_out_node(self, node):
            """ Remove an outgoing node  """
            if node in self.out_nodes:
                self.out_nodes.remove(node)
        
        def remove_in_node(self, node):
            """ Remove an incoming node """
            if node in self.in_nodes:
                self.in_nodes.remove(node)
    
    def __init__(self, strIter=[], 
                     k=None,  
                     edge_cutoff=None, 
                     node_cutoff=None, 
                     circularize=False, 
                     printout = False,
                     prune =True
                 ):
        ''' Build de Bruijn multigraph given string iterator and k-mer
            length k '''
        self.G = {}     # multimap from nodes to neighbors
        self.nodes = {} # maps k-1-mers to Node objects
        self.unbalanced = []
        self.semi = []
        self.num_of_reads = 0
        
        kmer_hist = get_km1mer_histogram(strIter, k)
        num_of_reads = len(strIter)
        
        for st in strIter:
            num_of_reads +=1
            if circularize:
                st += st[:k-1]
            for kmer, km1L, km1R in self.chop(st, k):
                nodeL, nodeR = None, None
                if km1L in self.nodes:
                    nodeL = self.nodes[km1L]
                else:
                    nodeL = self.nodes[km1L] = self.Node(km1L)
                    nodeL.freq = kmer_hist[km1L]/num_of_reads
                    
                if km1R in self.nodes:
                    nodeR = self.nodes[km1R]
                    
                else:
                    nodeR = self.nodes[km1R] = self.Node(km1R)
                    nodeR.freq = kmer_hist[km1R]/num_of_reads
                    
                #"""
                if nodeL not in self.G:
                    self.G[nodeL] = {}
                if nodeR not in self.G:
                    self.G[nodeR] = {}
                if nodeR not in self.G[nodeL]:
                    self.G[nodeL][nodeR] = 1/num_of_reads
                    nodeL.out_nodes.append(nodeR.km1mer)
                    nodeR.in_nodes.append(nodeL.km1mer)
                else:
                    self.G[nodeL][nodeR]+=1/num_of_reads
                #"""
        
        # Iterate over nodes; tally # balanced, semi-balanced, neither
        self.nsemi, self.nbal, self.nneither = 0, 0, 0
        # Keep track of head and tail nodes in the case of a graph with
        # Eularian walk (not cycle)
        self.head, self.tail = None, None
        self.heads, self.tails = [], [] 
        
        if prune:
            self.prune(edge_cutoff, node_cutoff, printout)            
             
        self.nsemi, self.nbal, self.nneither = 0, 0, 0
        
        for node in iter(self.nodes.values()):
            self.update_graph_info(node)
            
            
    def update_graph_info(self, node):
        if node.isBalanced():
            self.nbal += 1
        elif node.isSemiBalanced():
            if node.nin == node.nout + 1:
               self.tail = node
            if node.nin == node.nout - 1:
                self.head = node
            self.nsemi += 1
            self.semi.append(node)
        else:
            self.nneither += 1
            self.unbalanced.append(node)
        if  node.nin > 0 and node.nout == 0 \
            and node not in self.tails:
            self.tails.append(node)
        if node.nin == 0 and node.nout > 0\
            and node not in self.heads:
            self.heads.append(node)
                
    
    def nnodes(self):
        ''' Return # nodes '''
        return len(self.G)
    
    def nedges(self):
        ''' Return # edges '''
        total = 0
        for node in self.G:
            total += len(self.G[node])
        return total
    
    def add_node(self, node, neighbors=None):
        """ Add new node """
        if type(node) == str:
            self.G[node] = Node(node, neighbors) 
    
    def get_node(self, read):
        """ Get node for a given read """
        if read in self.G:
            return self.G[read]
    
    def remove_node(self, read):
        """ Remove node with a given read and update its neighbors"""
        if read in self.nodes:
            node = self.nodes.pop(read, None)
            
            for in_node in node.in_nodes:
                self.nodes[in_node].remove_out_node(read)
            
            for out_node in node.out_nodes:
                self.nodes[out_node].remove_in_node(read)
                
            del self.G[node]
        
        
        
    def remove_edge(self, src, dst):
        ''' Removes an edge from src node to dst node '''
        #TODO: aktualizacja nsemi nbal
        if dst in self.G[src]:
            del self.G[src][dst]
            dst.remove_in_node(src.km1mer)
            src.remove_out_node(dst.km1mer)
    
    
    def prune(self,  edge_cutoff, node_cutoff, printout= False):
        num_of_edges = self.nedges()
        nodes = deque(self.heads)
        
        for node in list(self.nodes):
            if node not in nodes:
                nodes.append(node)
        
        removed_nodes, removed_edges = 0, 0
        min_weight, min_overall = 1,1
        max_overall = 0
        
        while nodes:
            kmer = nodes.popleft()
            if kmer in self.nodes:
                src = self.nodes[kmer]
                
                neighbors = [node for node in self.G[src] if node.km1mer in self.nodes]
                
                if src.freq < node_cutoff:
                        self.remove_node(src.km1mer)
                        removed_nodes += 1
                else: 
                    while neighbors:
                        dst = neighbors.pop()
                        weight = self.G[src][dst]
                        
                        min_overall =min(min_overall, weight)
                        max_overall = max(max_overall, weight)
                        
                        #pruning low freq nodes and weak edges
                        if weight < edge_cutoff:
                            self.remove_edge(src, dst)
                            removed_edges += 1
                            min_weight = min(min_weight, weight)
                            max_weight = max(max_weight, weight)
                            
                        for node in (src, dst):
                            if node.freq < node_cutoff:
                                self.remove_node(node.km1mer)
                                removed_nodes += 1
                                
        if printout:                 
            print(f"\nPRUNING: {removed_nodes} nodes and {removed_edges} edges " \
                      f"have been removed. \nMinimum weight removed: {min_weight}." \
                      f"\nOverall minimum {min_overall:.02f}\nOverall maximum {max_overall}")
    
    def hasEulerianWalk(self):
        ''' Return true iff graph has Eulerian walk. '''
        return self.nneither == 0 and self.nsemi == 2
    
    def hasEulerianCycle(self):
        ''' Return true iff graph has Eulerian cycle. '''
        return self.nneither == 0 and self.nsemi == 0
    
    def isEulerian(self):
        ''' Return true iff graph has Eulerian walk or cycle '''
        # technically, if it has an Eulerian walk
        return self.hasEulerianWalk() or self.hasEulerianCycle()
    
    def eulerianWalkOrCycle(self):
        ''' Find and return sequence of nodes (represented by
            their k-1-mer labels) corresponding to Eulerian walk
            or cycle '''
        assert self.isEulerian()
        g = {k:list(self.G[k]) for k in self.G}
        
        if self.hasEulerianWalk():
            g = g.copy()
            g.setdefault(self.tail, []).append(self.head)
        # graph g has an Eulerian cycle
        tour = []
        src = next(iter(g.keys())) # pick arbitrary starting node
        
        def __visit(n):
            assert n in g, f"Brak {n.km1mer} w węzłach: {[str(n) for n in g]}"
            while len(g[n]) > 0:
                dst = g[n].pop()
                __visit(dst)
            tour.append(n)
        
        __visit(src)
        tour = tour[::-1][:-1] # reverse and then take all but last node
            
        if self.hasEulerianWalk():
            # Adjust node list so that it starts at head and ends at tail
            sti = tour.index(self.head)
            tour = tour[sti:] + tour[:sti]
        
        # Return node list
        return list(map(str, tour))
        
    def get_connected_component(self, node):
        queue = deque()
        visited = {}
        reachables = []
        queue.append(node)
     
        visited[node] = 1
     
        while len(queue) > 0:
            node_tmp = queue.popleft()
            reachables.append(node_tmp)
            
            for neighbor in node_tmp.in_nodes + node_tmp.out_nodes:
                if neighbor not in visited or not visited[neighbor]:
                    visited[neighbor] = 1
                    queue.append(self.nodes[neighbor])

        return reachables
    
    def build_subgraph(self, nodes):
        nodes = set(nodes)
        g = DeBruijnGraph(prune=False, printout=False)
        for node in nodes:
            g.nodes[node.km1mer] = node
            g.G[node] = self.G[node]
            if node in self.heads:
                g.heads.append(node)
            if node in self.tails:
                g.tails.append(node)
            g.update_graph_info(node)
        return g
    
    def contigs_from_subgraphs(self):
        ''' Generates connected components of a graph '''
        nodes = list(self.nodes.values())
        heads = self.heads
        
        while heads or nodes:
             if self.heads:
                 start = heads.pop()
             else:
                 start = nodes.pop()
             comp = self.get_connected_component(start)

             for kmer in comp:
                 if kmer in nodes:
                     nodes.remove(kmer)
                 
             if len(comp)>1:
                 subgraph = self.build_subgraph(comp)
                  
                 if subgraph.isEulerian():
                    walk = subgraph.eulerianWalkOrCycle() 
                    #print(start, 'Eulerian!', walk)
                    yield walk[0]+''.join(map(lambda x: x[-1], walk[1:]))
                    
    
    def extract_contigs_to_file(self, file):
        """ Write contigs to the output file """
        contig_num = 0
        with open(file, 'w') as f:
            for contig in self.contigs_from_subgraphs():
                print(f"CONTIG {contig_num}: {contig}")
                f.write(f">contig_{contig_num}\n{contig}\n")
                contig_num +=1 
        print(f"Number of extracted contigs: {contig_num}")
    
    def visualize(self, weights=False):
        """ Return string with graphviz representation.  If 'weights'
            is true, label edges corresponding to distinct k-1-mers
            with weights, instead of drawing separate edges for
            k-1-mer copies. """
        g = graphviz.Digraph(comment='DeBruijn graph', format='png')
        for node in iter(self.G.keys()):
            if node.isSemiBalanced():
                col = 'orange'
            elif node.isBalanced():
                col = 'black'
            else:
                col = 'red'
            g.node(node.km1mer,label=f"{str(node)} [{node.freq:.2f}]", color=col)
        for src, dsts in iter(self.G.items()):
            for dst in dsts:
                    g.edge(src.km1mer, dst.km1mer, label = f"{self.G[src][dst]:.2f}")
        g.view()

def parse_arguments():
    """ Parse arguments and validate input """
    ap = argparse.ArgumentParser()
    ap.add_argument("input", help="Input FASTA file containing reads to assemble")
    ap.add_argument("output", help="Name of the output file")
    ap.add_argument('-k','--kmer', required=False, default=75,  type=int,  help="K-mers length")
    args = ap.parse_args()
    
    if not os.path.isfile(args.input):
        print("File {} not found!".format(args.input))
        sys.exit()
    
    if os.path.isfile(args.output):
        resp=input("File {} already exists. Do you want to overwrite it? [[Y]\\N]".format(args.output))
        while resp.lower() not in ("", "n", "y"):
            resp=input("Unrecognized option. Do you want to overwrite {}? [[Y]\\N]".format(args.output))
        if resp.lower()=='n':
            sys.exit()
        else:
            os.remove(args.output)
            
    print("Input file: {}\nOutput file: {}\n".format(args.input, args.output))
    return args.input, args.output, args.kmer 

def parse_reads(file):
    return [str(read.seq) for read in SeqIO.parse(file, "fasta")]
 
def main():
    start = time.perf_counter()
    
    infile, outfile, k = parse_arguments()
    outfile  = "out/contigs.fasta"
    infile, k = "data/reads2.fasta", 10
    
    sequences = parse_reads(infile)

    G = DeBruijnGraph(sequences, k,  edge_cutoff = 0.00000001, node_cutoff = 0.00000001)
    
    assert [str(node) for node in G.nodes] == [str(node) for node in G.G], "Błąd w zawartości self.nodes/self.G!"
            
    if G.nnodes() <= 30:
        G.to_dot().view() 
    
    G.extract_contigs_to_file(outfile)
    stop = time.perf_counter()
    print(f"\nExecution time: {stop - start:0.4f} seconds")
     
def test():
    start = time.perf_counter()
    
    #infile, outfile, k = parse_arguments()
    outfile  = "training/contigs1.fasta"
    infile, k= "data/long_time1.fasta", 5
    infile, k = "data/long_time2.fasta", 12
    #infile, k = "data/reads1.fasta", 30
    #infile, k = "data/reads1.fasta", 15
    #infile, k = "data/happiness.fasta", 4
    
    sequences = parse_reads(infile)

    G = DeBruijnGraph(sequences, k,  
                      edge_cutoff = 0.2, 
                      node_cutoff = 0.2,
                      prune = True,
                      printout = True)
    
    assert [str(node) for node in G.nodes] == [str(node) for node in G.G], "Błąd w zawartości self.nodes/self.G!"

    print(f"\nReads: {G.num_of_reads}")
    print(f"\nNodes: {G.nnodes()}")
    print(f"Heads: {len(G.heads)}")
    print(f"Tails: {len(G.tails)}")
    print(f"\nBalanced nodes:{G.nbal}")
    print(f"Semi-balanced nodes:{G.nsemi}")
    print(f"Unbalanced nodes: {G.nneither}\n") 
        
    
    if G.nnodes() <= 300:
        G.visualize()
    
    G.extract_contigs_to_file(outfile)
    stop = time.perf_counter()
    print(f"\nExecution time: {stop - start:0.4f} seconds")

    
if __name__ == '__main__':
	test()
	
