#!/usr/bin/env python3

#required libraries
import argparse
import os
import sys
from Bio import SeqIO
import time
from collections import deque

#optional
try:
    import graphviz
    has_graphviz = True
except ImportError:
    has_graphviz = False


def parse_arguments():
    """ Parse arguments and validate input """
    ap = argparse.ArgumentParser(description = "De novo assembly algorithm based on DBG (De Bruijn graph) strategy.")
    
    ap.add_argument("input", help="Input FASTA file containing reads to assemble")
    ap.add_argument("output", help="Name of the output file")
    ap.add_argument('-k','--km1mer_len', required=False, default=20,  type=int, help="K-mer length, default: 20") 
    ap.add_argument('-c','--corr_threshold', required=False, default=2,  type=int, help="Frequency threshold for error_correction, default: 2")
    ap.add_argument('-m', '--cutoff_multip', required=False, default=0.4,  type=float, help="Mutliplier for the cutoff threshold. "\
                    "All edges and nodes with frequencies/weights lower than the cutoff_multipl*average will be removed, default: 0.4")
    ap.add_argument('--no_pruning', required=False, action='store_false', default=True, help="Run without pruning")                                                                                        
    ap.add_argument('--no_error_correction', required=False, action='store_false', default=True, help="Run without error correction")
    ap.add_argument('-v', '--visualize', required=False, action='store_true', help="Visualize graph (if # of nodes is less than 50)") 

    args = ap.parse_args()
    
    if not os.path.isfile(args.input):
        print("File {} not found!".format(args.input))
        sys.exit()
    
    if os.path.isfile(args.output):
        resp=input("File {} already exists. Do you want to overwrite it? [[Y]\\N]".format(args.output))
        while resp.lower() not in ("", "n", "y"):
            resp=input("Unrecognized option. Do you want to overwrite {}? [[Y]\\N]".format(args.output))
        if resp.lower()=='n':
            sys.exit()
        else:
            os.remove(args.output)
            
    print(f"\nInput file: {args.input}\nOutput file: {args.output}\n")
    return args 

def parse_reads(file):
    ''' Returns a list of reads from a file '''
    return [str(read.seq) for read in SeqIO.parse(file, "fasta")]

def get_km1mer_histogram(reads, k):
    ''' Return k-mer count '''
    kmerhist = {}
    if reads:
        for read in reads:
            counted = []
            for kmer in [ read[i:i+k-1] for i in range(len(read)+1-(k-1)) ]:
                if kmer not in counted:
                    counted.append(kmer)
                    kmerhist[kmer] = kmerhist.setdefault(kmer, 0) + 1
    return kmerhist

def get_nneighbors(kmer, alpha = 'ACTG'):
    ''' Generate all neighbors at Hamming distance 1 from kmer '''
    neighbors = []
    for j in range(len(kmer)-1, -1, -1):
        oldc = kmer[j]
        for c in alpha:
            if c == oldc: continue
            neighbors.append(kmer[:j] + c + kmer[j+1:])
    return neighbors

def correct_kmer(read, k, kmerhist, thresh):
    ''' Return an error-corrected version of read.  k = k-mer length.
        kmerhist is kmer count map.  alpha is alphabet.  thresh is
        count threshold above which k-mer is considered correct. '''
    new_read = read
    # Iterate over k-mers in read
    for i in range(0, len(read)+1-(k-1)):
        kmer = read[i:i+k-1]
        # If k-mer is infrequent...
        freq = kmerhist.get(kmer, 0)
        if freq <= thresh:
            # Look for a frequent neighbor
            for newkmer in get_nneighbors(kmer):
                new_freq = kmerhist.get(newkmer, 0)
                if new_freq > thresh:
                    if  kmer in kmerhist:
                        if  kmerhist.get(kmer, 0) <=1: 
                            del kmerhist[kmer] 
                        else:
                            kmerhist[kmer]-=1
                    kmerhist[newkmer] = kmerhist.setdefault(newkmer, 0) + 1
                    new_read = new_read[:i] + newkmer + new_read[i+k-1:]
                    break
        
    # Return possibly-corrected read
    return new_read


class DeBruijnGraph:
    ''' De Bruijn directed multigraph built from a collection of
        strings. User supplies strings and k-mer length k.  Nodes
        are k-1-mers.  An Edge corresponds to the k-mer that joins
        a left k-1-mer to a right k-1-mer. '''
 
    @staticmethod
    def chop(st, k):
        ''' Chop string into k-mers of given length '''
        for i in range(len(st)-(k-1)):
            yield (st[i:i+k], st[i:i+k-1], st[i+1:i+k])
    
    class Node:
        ''' Node representing a k-1 mer.  Keep track of # of
            incoming/outgoing edges so it's easy to check for
            balanced, semi-balanced. '''
        
        def __init__(self, km1mer):
            self.km1mer = km1mer
            self.in_nodes = []
            self.nout = 0
            self.freq = 0
            
        @property
        def nin(self):
            return len(self.in_nodes)
        
        def isSemiBalanced(self):
            return abs(self.nin - self.nout) == 1
        
        def isBalanced(self):
            return self.nin == self.nout
        
        def __hash__(self):
            return hash(self.km1mer)
        
        def __str__(self):
            return self.km1mer
        
        def print_node(self):
            return f"Node {self.km1mer}\n  -freq: {self.freq}" + \
                    f"\n  -in nodes: {[str(n) for n in self.in_nodes]} {self.nin}"+ \
                    f"\n  -out nodes: {self.nout} " +\
                    f"\n  -is Balanced: {self.isBalanced()}" +\
                    f"\n  -is Semi-Balanced: {self.isSemiBalanced()}" 
        
        
        def remove_in_node(self, node):
            """ Remove an incoming node """
            if node in self.in_nodes:
                self.in_nodes.remove(node)
                
    
    def __init__(self, strIter=[], k=None,  circularize = False, printout = False,
                 error_correction = False, corr_thresh = 1, prune = False, multip = 1):
        ''' 
        Build de Bruijn multigraph given the following parameters:
            - strIter: string iterator with reads to be analyzed
            - k: length of considered kmers
            - error_correction: perform error correction
            - corr_thresh: threshold of kmer occurances for error correction
            - prune: filter out infrequent nodes and edged
            - multip: multiplier average weight and frequency to establish 
                      cut-off values for pruning
            - printout: print information during execution
            
        '''
        self.G = {}     # multimap from nodes to neighbors
        self.nodes = {} # maps k-1-mers to Node objects
        self.printout = printout
        
        if error_correction:
            kmer_hist = get_km1mer_histogram(strIter, k) #pre-correction
            self.error_correction(strIter, k, kmer_hist, corr_thresh)
        
        self.num_of_reads = len(strIter)
        
        kmer_hist = get_km1mer_histogram(strIter, k) #kmers from corrected reads
        
        if self.printout: print(f"\nBUILDING dBG GRAPH WITH k={k}...")
        
        for st in strIter:
            if circularize:
                st += st[:k-1]
            for kmer, km1L, km1R in self.chop(st, k):
                nodeL, nodeR = None, None
                if km1L in self.nodes:
                    nodeL = self.nodes[km1L]
                else:
                    nodeL = self.nodes[km1L] = self.Node(km1L)
                    nodeL.freq = kmer_hist[km1L]/self.num_of_reads
                    
                if km1R in self.nodes:
                    nodeR = self.nodes[km1R]
                    
                else:
                    nodeR = self.nodes[km1R] = self.Node(km1R)
                    nodeR.freq = kmer_hist[km1R]/self.num_of_reads
                    
                #"""
                if nodeL not in self.G:
                    self.G[nodeL] = {}
                if nodeR not in self.G:
                    self.G[nodeR] = {}
                if nodeR not in self.G[nodeL]:
                    self.G[nodeL][nodeR] = 1/self.num_of_reads
                    nodeL.nout +=1
                    nodeR.in_nodes.append(nodeL)
                else:
                    self.G[nodeL][nodeR]+=1/self.num_of_reads
                #"""
        
        # Iterate over nodes; tally # balanced, semi-balanced, neither
        self.nsemi, self.nbal, self.nneither = 0, 0, 0
        # Keep track of head and tail nodes in the case of a graph with
        # Eularian walk (not cycle)
        self.head, self.tail = None, None
        self.heads = []
        
        
        if prune:
            avg_freq, avg_weight = self.get_average_freq_and_weight()
            self.prune(multip*avg_weight, multip*avg_freq, printout)    
        
             
        for node in self.G:
            list1 = []
            for node2 in self.G:
                if node in self.G[node2]:
                    list1.append(node2)   
        
        self.nsemi, self.nbal, self.nneither = 0, 0, 0
        
        for node in iter(self.nodes.values()):
            self.update_graph_info(node)
        
    def error_correction(self, reads, k, hist, threshold):
        ''' 
        Error correction - subsctitutes k-1mers with frequency < threshold
        in a read with a more frequent neighbor
        '''
        if self.printout: 
            print(f"ERROR CORRECTION...\nCorrection threshold: {threshold}")
            
        corrected = 0
        for i, read in enumerate(reads):
            newread = correct_kmer(read, k, hist, threshold)
            if not newread:
                reads.remove(read)
            if not newread == read:
                corrected +=1
                reads[i] = newread
                
        if self.printout:
            print(f"Done. Corrected reads: {corrected}")
        
    def update_graph_info(self, node):
        '''Update weights, frequencies and types of nodes in a graph '''
        if node.isBalanced():
            self.nbal += 1
        elif node.isSemiBalanced():
            if node.nin == node.nout + 1:
                self.tail = node
            if node.nin == node.nout - 1:
                self.head = node
            self.nsemi += 1
        else:
            self.nneither += 1
        if node.nin == 0 and node.nout > 0\
            and node not in self.heads:
            self.heads.append(node)
                

    def nnodes(self):
        ''' Return # nodes '''
        return len(self.G)
    
    def nedges(self):
        ''' Return # edges '''
        total = 0
        for node in self.G:
            total += len(self.G[node])
        return total
    
    def get_average_freq_and_weight(self):
        ''' Return the average node frequency and edge weight '''
        freq_total, weight_total = 0, 0
        for node in self.G:
            freq_total += node.freq
            for neighbor in self.G[node]:
                weight_total += self.G[node][neighbor]
                
        avg_freq, avg_weight = 0, 0
        
        nnod, nedg  = self.nnodes(), self.nedges()
        if nnod:
            avg_freq = freq_total/nnod
        if nedg:
            avg_weight = weight_total/nedg
            
        return avg_freq, avg_weight
            
    def add_node(self, node, neighbors=None):
        """ Add new node """
        if type(node) == str:
            self.G[node] = Node(node, neighbors) 
    
    def get_node(self, km1mer):
        """ Get node for a given read """
        if read in self.G:
            return self.G[km1mer]
    
    def remove_node(self, km1mer):
        """ Remove node with a given read and update its neighbors"""
        if km1mer in self.nodes:
            node = self.nodes.pop(km1mer, None)
            
            for in_node in node.in_nodes:
                del self.G[in_node][node]
                self.nodes[in_node.km1mer].nout -=1
            
            for out_node in self.G[node]:
                out_node.remove_in_node(node)
                
            del self.G[node]
        
    def remove_edge(self, src, dst):
        ''' Removes an edge from src node to dst node '''
        if dst in self.G[src]:
            del self.G[src][dst]
            dst.remove_in_node(src)
            src.nout -=1
    
    def prune(self,  edge_cutoff, node_cutoff, printout= False):
        '''
        Graph correction - removes nodes and edges below 0.5 * average 
        frequency or weight (respectively) 
        '''
        if printout: 
            print(f"\nPRUNING...\nNode frequency threshold: {node_cutoff:.4f}"\
                  f"\nEdge weight threshold: {edge_cutoff:.4f}")
        
        num_of_edges = self.nedges()
        nodes = deque(self.heads)
        
        for node in list(self.nodes):
            if node not in nodes:
                nodes.append(node)
        
        removed_nodes, removed_edges = 0, 0
        min_weight, min_overall = 1,1
        max_overall = 0
        
        while nodes:
            kmer = nodes.popleft()
            if kmer in self.nodes:
                src = self.nodes[kmer]
                
                neighbors = [node for node in self.G[src] if node.km1mer in self.nodes]
                
                if src.freq < node_cutoff:
                        self.remove_node(src.km1mer)
                        removed_nodes += 1
                else: 
                    while neighbors:
                        dst = neighbors.pop()
                            
                        if dst.freq < node_cutoff:
                                self.remove_node(dst)
                                removed_nodes += 1
                        else:
                            weight = self.G[src][dst]
                            
                            min_overall =min(min_overall, weight)
                            max_overall = max(max_overall, weight)
                            
                            #pruning low freq nodes and weak edges
                            if weight < edge_cutoff:
                                self.remove_edge(src, dst)
                                removed_edges += 1
                                min_weight = min(min_weight, weight)          
        if printout:   
            print(f"{removed_nodes} nodes and {removed_edges} edges have been removed.")
    
    def hasEulerianWalk(self):
        ''' Return true iff graph has Eulerian walk. '''
        return self.nneither == 0 and self.nsemi == 2
    
    def hasEulerianCycle(self):
        ''' Return true iff graph has Eulerian cycle. '''
        return self.nneither == 0 and self.nsemi == 0
    
    def isEulerian(self):
        ''' Return true iff graph has Eulerian walk or cycle '''
        # technically, if it has an Eulerian walk
        return self.hasEulerianWalk() or self.hasEulerianCycle()
    
    def eulerianWalkOrCycle(self):
        ''' 
        Find and return sequence of nodes (represented by
        their k-1-mer labels) corresponding to Eulerian walk
        or cycle 
        '''
        assert self.isEulerian()
        g = {k:list(self.G[k]) for k in self.G}
        
        if self.hasEulerianWalk():
            g = g.copy()
            g.setdefault(self.tail, []).append(self.head)
        # graph g has an Eulerian cycle
        tour = []
        src = next(iter(g.keys())) # pick arbitrary starting node
        
        def __visit(n):
            while len(g[n]) > 0:
                dst = g[n].pop()
                __visit(dst)
            tour.append(n)
        
        __visit(src)
        tour = tour[::-1][:-1] # reverse and then take all but last node
            
        if self.hasEulerianWalk():
            # Adjust node list so that it starts at head and ends at tail
            sti = tour.index(self.head)
            tour = tour[sti:] + tour[:sti]
        
        # Return node list
        return list(map(str, tour))
    
    def eulerianWalkOrCycle(self):
        ''' 
        Find and return sequence of nodes (represented by 
        their k-1-mer labels) corresponding to Eulerian walk
        or cycle 
        '''
        assert self.isEulerian()
        g = {k:list(self.G[k]) for k in self.G}
        
        if self.hasEulerianWalk():
            g = g.copy()
            g.setdefault(self.tail, []).append(self.head)
        # graph g has an Eulerian cycle
        tour = []
        src = next(iter(g.keys())) # pick arbitrary starting node
        queue = deque()
        queue.append(src)
        
        while queue:
            dst = queue.popleft()
            tour.append(dst)
            if len(g[dst]):
                new_src = g[dst][0]
                g[dst].remove(new_src)
                queue.append(new_src)
        
        tour = tour[:-1] 
        
        if self.hasEulerianWalk():
            # Adjust node list so that it starts at head and ends at tail
            sti = tour.index(self.head)
            tour = tour[sti:] + tour[:sti]
       
        #print([n.km1mer for n in tour])
        # Return node list
        return list(map(str, tour))
        
    def get_connected_component(self, node):
        ''' Iterate through connected components of a graph '''
        queue = deque()
        visited = {}
        reachables = []
        queue.append(node)
     
        visited[node] = 1
     
        while len(queue) > 0:
            node_tmp = queue.popleft()
            reachables.append(node_tmp)
            
            for neighbor in node_tmp.in_nodes + list(self.G[node_tmp]):
                if neighbor.km1mer not in visited or not visited[neighbor.km1mer]:
                    visited[neighbor.km1mer] = 1
                    queue.append(self.nodes[neighbor.km1mer])

        return reachables
    
    def build_subgraph(self, nodes):
        ''' Build a subgraph from a given list of nodes '''
        nodes = set(nodes)
        g = DeBruijnGraph(prune=False, printout=False)
        for node in nodes:
            g.nodes[node.km1mer] = node
            g.G[node] = self.G[node]
            g.update_graph_info(node)
        return g
    
    def contigs_from_subgraphs(self):
        ''' Generate connected components of a graph '''
        nodes = list(self.nodes.values())
        heads = self.heads
        
        while heads or nodes:
             if self.heads:
                 start = heads.pop()
             else:
                 start = nodes.pop()
             comp = self.get_connected_component(start)

             for kmer in comp:
                 if kmer in nodes:
                     nodes.remove(kmer)
                 
             if len(comp)>1:
                 subgraph = self.build_subgraph(comp)
                  
                 if subgraph.isEulerian():
                    walk = subgraph.eulerianWalkOrCycle() 
                    yield walk[0]+ "".join(map(lambda x: x[-1], walk[1:])) 
                    
    def extract_contigs_to_file(self, file):
        ''' Write contigs to the output file '''
        if self.printout: print("\nCONTIG EXTRACTION...")
        num_of_valid = 0
        max_len = -1
        contig_num = 0
        with open(file, 'w') as f:
            for contig in self.contigs_from_subgraphs():
                #print(f"CONTIG {contig_num}: {contig}")
                f.write(f">contig_{contig_num}\n{contig}\n")
                #print(f"CONTIG: {contig}")
                contig_num +=1 
                if len(contig)>=300: num_of_valid+=1
                max_len = max(max_len, len(contig))
        if self.printout: print(f"Done. Number of extracted contigs: {contig_num}\n"\
              f"Number of valid contigs: {num_of_valid}\n"\
              f"Maximum contig length: {max(max_len, 0)}\n")
    
    def visualize(self, weights=False):
        ''' 
        Return string with graphviz representation.  If 'weights'
        is true, label edges corresponding to distinct k-1-mers
        with weights, instead of drawing separate edges for
        k-1-mer copies. 
        '''
        if has_graphviz:
            g = graphviz.Digraph(comment='DeBruijn graph', format='png')
            for node in iter(self.G.keys()):
                if node.isSemiBalanced():
                    col = 'orange'
                elif node.isBalanced():
                    col = 'black'
                else:
                    col = 'red'
                g.node(node.km1mer,label=f"{str(node)} [{node.freq:.2f}]\nin:{node.nin}\nout:{node.nout}", color=col)
            for src, dsts in iter(self.G.items()):
                for dst in dsts:
                        g.edge(src.km1mer, dst.km1mer, label = f"{self.G[src][dst]:.2f}")
            g.view()
        else:
            print("Please install graphviz library to display graph visualization.")


def main():
    start = time.perf_counter()
    
    args = parse_arguments()

    #Get reads
    sequences = parse_reads(args.input)
    
    #Build dBG graph for an input
    G = DeBruijnGraph(sequences, 
                      k = args.km1mer_len,  
                      corr_thresh = args.corr_threshold, 
                      error_correction = args.no_error_correction,
                      prune = args.no_pruning,
                      multip = args.cutoff_multip,
                      printout = True)
       
    #Find Eulerian subgraphs and extract contigs
    G.extract_contigs_to_file(args.output)
    
    if args.visualize and G.nnodes() <= 50:
       G.visualize()
    
    print(f"Execution time: {time.perf_counter() - start:0.4f} seconds")
    
if __name__ == '__main__':
	main()
	
