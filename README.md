# Assembly

#### Description:

Assemble.py script contain an implementation of a sequence assembly algorithm based on DBG (De Bruijn graph) strategy. It takes a FASTA file containg reads to be assembled as an input and saves the results to a FASTA file given as a second argument. 


By default, script performs error correction which first creates a dictionary with kmer frequencies and then use it to substitute infrequent kmers in a read with its more frequent neighbor (at Hamming distace 1 from the original) and proceeds with analysis using corrected reads. A correction threshold for kmer frequencies can be set by using -c parameter.

After the de Brujin graph for a given input is built, the average of edge weights and node frequencties are calculated, multiplied by a cutoff_multip argument and in such form used as a cut-off value for pruning (e.g. a node with scaled frequency lower than average of all frequencies * cut-off multiplier will be removed).

Then, connected components are extracted from the graph and if a subgraph is Eulerian, a contig corresponding to its Eulerian path/cycle is saved to output file.

Both error correction and pruning can be excluded from a run using arguments described below.

#### Authors: 
 
Przemysław Kiljan, Małgorzata Kukiełka

## Requirements

- Python 3.6 or higher

- Python libraries: argparse, os, graphviz, time, Bio, collections 

## Usage


```bash
./assemble reads1.fasta contigs1.fasta -k 15 -c 1
./assemble reads.fasta out_contigs.fasta -k 30 -c 3 -t 5--no_pruning
```
## Arguments 
Positional arguments:

```
  input                 Input FASTA file containing reads to assemble
  output                Output file
```

Optional arguments:

```
  -h, --help            Show help message and exit
  
  -k,--km1mer_len       K-mer length, default: 20
  
  -c,--corr_threshold   Frequency threshold for error_correction, default: 2
                       
  -m, --cutoff_multip   Mutliplier for the cutoff threshold. All edges and
                        nodes with frequencies/weights lower than the
                        cutoff_multipl*average will be removed, by default: 0.5
                        
  --no_pruning          Run without pruning
                        
  --no_error_correction Run without error correction
                        
  -v, --visualize       Visualize graph using graphviz (if # of nodes is less than 50)
```

## Sources

"De Bruijn Graph assembly" materials by Ben Langmead

- https://www.cs.jhu.edu/~langmea/resources/lecture_notes/assembly_dbg.pdf

- https://ocw.mit.edu/courses/biology/7-91j-foundations-of-computational-and-systems-biology-spring-2014/lecture-slides/MIT7_91JS14_Lecture6.pdf

- http://www.langmead-lab.org/teaching-materials/
