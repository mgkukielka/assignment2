#!/usr/bin/env python

import argparse, datetime
import sys, os
import math
import time
from Bio import SeqIO
from itertools import permutations
import graphviz

# python3 ./assembly_olc.py data/long_time.fasta output_contigs.fasta -m 5 -v

def create_output_directory(outdir):
    """ Create directory for output files """
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        print(f"Directory {outdir} created")

def parse_arguments():
    """ Parse arguments and check input """
    ap = argparse.ArgumentParser()
    ap.add_argument("input", type=str, help="Input FASTA file with reads to analyse")
    ap.add_argument("output", type=str, help="Name of the output file")
    ap.add_argument('-o', '--outdir', type=str, required=False, default='out', help="Name of the output file")
    ap.add_argument('-m','--min_overlap', required=False, default=50,  type=int,  help="Minimum length of overlap")
    ap.add_argument('-v', required=False, action="store_true",   help="Visualize graph (up to 30 vertices), default: False")
    args = ap.parse_args()
    
    create_output_directory(args.outdir)
    
    outfile = os.path.join(args.outdir,args.output)
    
    if not os.path.isfile(args.input):
        print(f"File {args.input} not found!")
        sys.exit()
    
    if os.path.isfile(outfile):
        resp=input(f"File {outfile} already exists. Do you want to overwrite it? [[Y]\\N]")
        while resp.lower() not in ("", "n", "y"):
            resp=input(f"Unrecognized option. Do you want to overwrite {outfile}? [[Y]\\N]")
        if resp.lower()=='n':
            quit()
        else:
            os.remove(outfile)
            
    print(f"\nINPUT: {args.input}\nOUTPUT: {outfile}")
    return args.input, outfile, args.min_overlap, args.v, args.outdir


def overlap(str1, str2, min_length):
    """ Return length of longest suffix of 'str1' matching
        a prefix of 'str2' that is at least 'min_length'
        characters long.  If no such overlap exists,
        return 0. """
    
    #TODO: SuffixTree/ FM-index?    
    
    start = 0  # start all the way at the left
    while True:
        start = str1.find(str2[:min_length], start)  # look for b's suffx in a
        if start == -1:  # no more occurrences to right
            return 0
        # found occurrence; check for full suffix/prefix match
        if str2.startswith(str1[start:]):
            return len(str1)-start
        start += 1  # move just past previous match
        

def parse_reads(file):
    """ Extract reads from fasta file """
    return [str(read.seq) for read in SeqIO.parse(file, "fasta")]

def write_contigs_to_file(contigs, file):
    """ Write contigs to the output file """
    with open(file, 'w') as f:
        f.write("\t".join(contigs))
  
class Node:
    def __init__(self, read, neighbors = None):
        """ Node of a directed weighted graph """
        self.read=read
        self.in_nodes = []
        self.best = (None, 0) #neighbour with max overlap 
        self.out_nodes = {}
        
        if neighbors:
            for neighbor in neighbors:
                self.add_out_node(neighbor, neighbors[neighbor])

    def __hash__(self):
        return hash(self.read)
        
    def add_out_node(self, node, weight):
        """ Add a new neighbor and update node with max overlap if necessary """
        self.out_nodes[node] = weight
        
        if weight > self.best[1]:
            self.best = (node, weight)
            
    def remove_out_node(self, node):
        """ Remove an outgoing node and update node with max overlap if necessary """
        if node in self.out_nodes:
            del self.out_nodes[node]
        
        if node == self.best[0]:
            self.update_max_overlap_node()
        
    def remove_in_node(self, node):
        """ Remove an incoming node """
        if node in self.in_nodes:
            self.in_nodes.remove(node)
        
    def get_max_overlap_node(self):
        return self.best
    
    def update_max_overlap_node(self):
        if self.out_nodes:
            max_overlap_node = max(self.out_nodes, 
                                   key=lambda k: self.out_nodes[k])
            self.best = max_overlap_node, self.out_nodes[max_overlap_node]
        else:
            self.best = None, 0
    
    def is_reachable(self, node):
        return node==self.read or node in self.out_nodes
    
    def __str__(self):
        return f"Node: {self.seq}\nNeighbors: {self.neighbors}\nIncoming edges: {self.nin}"
    

                    
        
class OverlapGraph:
    
    def __init__(self, reads, min_overlap):
        self.V = {}
        self.num_edges = 0
        self.min_overlap = min_overlap
        self.build_graph(reads)
    
    
    def add_node(self, node, neighbors=None):
        """ Add new node """
        self.V[node] = Node(node, neighbors) 
    
    def get_node(self, read):
        """ Get node for a given read """
        if read in self.V:
            return self.V[read]
    
    def remove_node(self, read):
        """ Remove node with a given read and update its neighbors"""
        if read in self.V:
            node = self.V.pop(read, None)
            
            for in_node in node.in_nodes:
                self.V[in_node].remove_out_node(read)
            
            for out_node in node.out_nodes:
                self.V[out_node].remove_in_node(read)
        
    def add_edge(self, v1, v2, weight):
        """ Add new edge and update number of incoming edges in 
            the second node"""
        if v1 not in self.V:
            self.add_node(v1, {v2: weight})
        else: 
            self.V[v1].add_out_node(v2, weight)
        
        if v2 not in self.V:
            self.add_node(v2)
            
        self.V[v2].in_nodes.append(v1) 
        self.num_edges +=1
        
    def remove_edge(self, v1, v2):
        if v1 in self.V:
            start = self.V[v1].remove_out_node(v2)
        
        if v2 in self.V:
            end = self.V[v2].remove_in_node(v1)
        
        self.num_edges -=1
    
    def build_graph(self, reads):
        for read1, read2 in permutations(reads, 2):
            self.update_graph(read1, read2)
        
        print(f"\nAn overlap graph have been built successfully and \
              consists of {len(self.V)} nodes and {self.num_edges} edges.")
    
    def update_graph(self, r1, r2):
        """ Create new edge and update number of incoming edges in 
            the second node """
        olen = overlap(r1, r2, self.min_overlap)
        
        if olen > 0:
            self.add_edge(r1, r2, olen)
            
            
    def is_node_reachable(self, from_node, to_node, degree=1):
        if degree > 2 or from_node not in self.V or to_node not in self.V:
            return False
        elif from_node==to_node:
            return True
        else:
            from_N = self.V[from_node]
            to_N = self.V[to_node]
            for node in from_N.out_nodes:
                if node == to_node or self.is_node_reachable(node, to_node, degree+1):
                    return True
            else:
                return False
            
            
    
    def transitive_reduction(self):
        """ Remove transitively inferrable edges """
        removed = 0
        keys = list(self.V)
        for node in keys:
            out_nodes_list = list(self.V[node].out_nodes)[:]
            for out_node in out_nodes_list:
                nodes_to_check = out_nodes_list[:]
                while nodes_to_check:
                    out_node2 = nodes_to_check.pop()
                    if out_node != out_node2 and self.is_node_reachable(out_node2, out_node):
                        self.remove_edge(node, out_node)
                        removed += 1
                        break
                    
        print(f"\nTRANSITIVE REDUCTION: {removed} edges have been removed.")
                
        
    def go_down_nonbranching_path(self, start):
        """ Emit contig corresponding to the non-branching stretch
            starting from a given node """
        # TODO: poprawić żeby wybierało rzeczywiście non-branching, na razie 
        # idzie po węzłach o nawiększym nałożeniu 
        
        contig = start
        node = self.V[start]
        while node.out_nodes:
            new_node, olap = node.best
            contig += new_node[olap:]
            node = self.V[new_node]
            
            if node.read == start: break 
            
        return contig
    

    def emit_contigs(self, file, print_out = False):
        """ Emimt contigs from an overlap graph and save them to file """
        #TODO: poprawić wyszukiwanie contigów, na razie zaczyna od węzła bez 
        #wchodzących krawędzi i wybiera najlepszy (max overlap) węzeł
        
        contigs = 0
        zero_in_nodes = [node for node in self.V if len(self.V[node].in_nodes)==0]
        with open(file, 'w+') as of:
            if zero_in_nodes:
                for start_node in zero_in_nodes:
                    new_contig = self.go_down_nonbranching_path(start_node)
                    if len(new_contig)>0: 
                        if print_out:
                            print(f"\nCONTIG {contigs}: {new_contig}")
                        contigs+=1
                        of.write(new_contig+'\t') 
                        

    
    def visualize(self, outdir):
        """ Visualize overlap graph using graphviz library """
        os.chdir(outdir)
        g = graphviz.Digraph(comment='OLC', format='png')
        
        for read in self.V:
            N = self.V[read]
            g.node(read, label=f"{read}  [{len(N.in_nodes)}]")
            adj_nodes = self.V[read].out_nodes
            for neighbor in adj_nodes:
                olen = adj_nodes[neighbor]
                if N.best[0]==neighbor:
                    col='red'
                else:
                    col='black'
                g.edge(read, neighbor, label=str(olen), color=col)
        
        g.view()
        os.chdir("..")


def main():
    
    start = time.perf_counter()
 
    infile, outfile, min_overlap, visual, outdir = parse_arguments()
    
    #nadpisanie parametrów do testów
    #infile = "data/long_time.fasta"   
    #infile = "data/reads_tst.fasta"  
    #min_overlap = 4
    #visual = True 
    
    reads = parse_reads(infile)
    
    #find FM-index for input reads
    
    #______ OVERLAP ______
    
    #TODO: change overlap() to using FM index/SuffixTrees (?)
    
    #build overlap graph
    G = OverlapGraph(reads, min_overlap)
    
    #______ LAYOUT ___+___
    G.transitive_reduction()
    
    #TODO: handle subgraphs that are spurious, 
    #e.g. because of sequencing error
    
    #_____ CONSENSUS _____
    
    #TODO: majorty consensus for each nucleotide position in a contig
    
    G.emit_contigs(outfile, print_out = True)
    
    #write_contigs_to_file(contigs, outfile)
    
    if visual:
        if len(G.V)<=30:
            G.visualize(outdir)
        else:
            print(f"Visualization stopped, {len(G.V)} vertices found.")
    
    stop = time.perf_counter()
    print(f"\nExecution time: {stop - start:0.4f} seconds")

def test_transitive_reduction():
    
    infile = "data/happiness.fasta"   
    outfile = "out/happiness_test.fasta"
    outdir = "out"
    min_overlap = 4
    
    create_output_directory(outdir)
     
    reads = parse_reads(infile)
    G = OverlapGraph(reads, min_overlap)
    G.remove_edge('HAPPIN', 'PPINES')
    
    assert G.is_node_reachable('APPINES','PINESS') and \
        not G.is_node_reachable('APPINES','APPINS'), \
        "ERROR: is_node_reachable() doesn't work properly.";
        
    G.transitive_reduction()
    G.emit_contigs(outfile, print_out=True)
    
    G.visualize(outdir)
    
    assert G.num_edges == 5, "ERROR: Transitively inferrable edges found."
    assert len(G.V) == 6, "ERROR: Incorrect number of vertices."
     
if __name__ == '__main__':
	main()
    #test_transitive_reduction()	
